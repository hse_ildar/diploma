import tensorflow as tf
import numpy as np

# Load the C++ code dataset
with open('../../data/rnn/2_cpp_dataset.txt') as f:
    data = f.readlines()

# Tokenize the code snippets
word_to_id = {}
id_to_word = {}
for line in data:
    tokens = line.strip().split()
    for token in tokens:
        if token not in word_to_id:
            word_to_id[token] = len(word_to_id)
            id_to_word[len(id_to_word)] = token

# Convert the code snippets to integer sequences
X = []
y = []
for line in data:
    tokens = line.strip().split()
    x = [word_to_id[token] for token in tokens]
    X.append(x)
    y.append(1 if 'bug' in line else 0)

# Pad the integer sequences to a fixed length
max_len = max(len(x) for x in X)
X = np.array([x + [0]*(max_len - len(x)) for x in X])
y = np.array(y)


# Define the RNN model
model = tf.keras.Sequential([
    tf.keras.layers.Embedding(input_dim=len(word_to_id), output_dim=64),
    tf.keras.layers.SimpleRNN(64),
    tf.keras.layers.Dense(1, activation='sigmoid')
])

# Compile the model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Train the model
model.fit(X, y, epochs=10, batch_size=32)

# Evaluate the model on a test set
with open('mock/2_cpp_test_dataset.txt') as f:
    test_data = f.readlines()

X_test = []
y_test = []
for line in test_data:
    tokens = line.strip().split()
    x = [word_to_id.get(token, 0) for token in tokens]
    X_test.append(x)
    y_test.append(1 if 'bug' in line else 0)

X_test = np.array([x + [0]*(max_len - len(x)) for x in X_test])
y_test = np.array(y_test)

loss, accuracy = model.evaluate(X_test, y_test)
print(f'Test loss: {loss:.4f}, Test accuracy: {accuracy:.4f}')

