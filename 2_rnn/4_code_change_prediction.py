import pandas as pd
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Embedding

# Load the code snippets from a CSV file
data = pd.read_csv('../../data/rnn/4_code_snippets.csv')
dataset = data['code'].values.tolist()

# Define the vocabulary size and sequence length
vocab_size = 10000
seq_len = 100

# Create a dictionary to map words to integers
word_to_int = create_word_to_int_dict(dataset, vocab_size)

# Convert the dataset to integer sequences
int_sequences = convert_to_int_sequences(dataset, word_to_int, seq_len)

# Split the dataset into training and validation sets
X_train, y_train, X_val, y_val = split_dataset(int_sequences)

# Define the RNN model architecture
model = Sequential()
model.add(Embedding(vocab_size, 128, input_length=seq_len))
model.add(LSTM(128))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Train the model
model.fit(X_train, y_train, validation_data=(X_val, y_val), epochs=10, batch_size=32)

# Use the model to predict the likelihood of a code change being accepted
new_code_snippet = "int x = 5;\nint y = 10;\nint z = x + y;"
new_code_int_sequence = convert_to_int_sequence(new_code_snippet, word_to_int, seq_len)
prediction = model.predict(np.array([new_code_int_sequence]))
if prediction > 0.5:
    print("The code change is likely to be accepted.")
else:
    print("The code change is likely to be rejected.")
