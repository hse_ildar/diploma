import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Embedding, LSTM, Dense
from tensorflow.keras.models import Sequential

# Load the dataset
data = pd.read_csv("../../data/rnn/1_cpp_dataset.csv")
labels = data['label']
code_snippets = data['code']

# Tokenize the code snippets
tokenizer = Tokenizer()
tokenizer.fit_on_texts(code_snippets)
sequences = tokenizer.texts_to_sequences(code_snippets)
word_index = tokenizer.word_index

# Pad the sequences
max_length = max([len(seq) for seq in sequences])
padded_sequences = pad_sequences(sequences, maxlen=max_length, padding='post')

# Define the RNN model
model = Sequential()
model.add(Embedding(len(word_index) + 1, 128, input_length=max_length))
model.add(LSTM(128, dropout=0.2, recurrent_dropout=0.2))
model.add(Dense(1, activation='sigmoid'))

# Compile the model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Train the model
model.fit(padded_sequences, labels, epochs=10, batch_size=32, validation_split=0.2)

# Test the model
test_snippet = "int x = 5;"
test_sequence = tokenizer.texts_to_sequences([test_snippet])
test_padded = pad_sequences(test_sequence, maxlen=max_length, padding='post')
prediction = model.predict(test_padded)
if prediction > 0.5:
    print("This code snippet contains a code smell or design violation.")
else:
    print("This code snippet is clean.")
