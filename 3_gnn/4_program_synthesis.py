import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import json

# Load the input features from the JSON file
with open('../../data/gnn/data4.json', 'r') as f:
    input_features = json.load(f)

# Compute the input dimensionality
input_dim = len(input_features[0])

# Define the GNN model
class GNN(keras.Model):
    def __init__(self, input_dim, hidden_dim, output_dim):
        super(GNN, self).__init__()
        self.gcn1 = layers.Dense(hidden_dim, activation='relu')
        self.gcn2 = layers.Dense(hidden_dim, activation='relu')
        self.output_layer = layers.Dense(output_dim)

    def call(self, inputs):
        x, adj = inputs
        x = self.gcn1(x)
        x = tf.matmul(adj, x)
        x = self.gcn2(x)
        x = tf.matmul(adj, x)
        x = self.output_layer(x)
        return x

# Define the problem specification
input_dim = 2
output_dim = 1
specification = lambda x: tf.reduce_sum(x, axis=1)

# Generate training data
X_train = tf.random.uniform((100, input_dim))
y_train = specification(X_train)

# Train the GNN model
model = GNN(input_dim, hidden_dim=16, output_dim=output_dim)
model.compile(optimizer='adam', loss='mse')
model.fit((X_train, adj), y_train, epochs=100)

# Generate code from the learned model
def generate_code(input_dim, output_dim, model):
    x = layers.Input(shape=(input_dim,))
    adj = layers.Input(shape=(input_dim, input_dim))
    y = model((x, adj))
    model = keras.Model(inputs=[x, adj], outputs=y)
    code = keras.backend.function([x, adj], y)
    return code

code = generate_code(input_dim, output_dim, model)
