import torch
import torch.nn.functional as F
from torch_geometric.nn import GCNConv, GNNExplainer

class GNNModel(torch.nn.Module):
    def __init__(self, num_features, num_classes):
        super(GNNModel, self).__init__()
        self.conv1 = GCNConv(num_features, 16)
        self.conv2 = GCNConv(16, num_classes)

    def forward(self, x, edge_index):
        x = F.relu(self.conv1(x, edge_index))
        x = self.conv2(x, edge_index)
        return x

# Load the dataset and split it into training and testing sets
# dataset is = {
#     'x': torch.FloatTensor([[0.0, 1.0, 0.0], [1.0, 0.0, 1.0], ...]), # Node features for each code element
#     'edge_index': torch.LongTensor([[0, 1], [1, 2], ...]).t(), # Edge indices for the graph connections between code elements
#     'y': torch.LongTensor([1, 0, 1, 0, ...]) # Labels for each code example (1 for defective, 0 for defect-free)
# }
dataset = load_dataset('../../data/gnn/data3.json')
train_dataset, test_dataset = split_dataset(dataset)

# Create a GNN model and optimizer
model = GNNModel(dataset.num_features, dataset.num_classes)
optimizer = torch.optim.Adam(model.parameters(), lr=0.01)

# Train the model for a specified number of epochs
for epoch in range(num_epochs):
    model.train()
    optimizer.zero_grad()
    out = model(train_dataset.x, train_dataset.edge_index)
    loss = F.nll_loss(out, train_dataset.y)
    loss.backward()
    optimizer.step()

    model.eval()
    with torch.no_grad():
        pred = model(test_dataset.x, test_dataset.edge_index).argmax(dim=1)
        acc = pred.eq(test_dataset.y).sum().item() / len(test_dataset.y)
        print(f'Epoch {epoch}, Test Acc: {acc:.4f}')
