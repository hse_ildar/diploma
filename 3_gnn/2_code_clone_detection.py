import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import MessagePassing
from torch_geometric.data import Data
from torch_geometric.utils import to_networkx
import networkx as nx
import json

# Define the GNN model for code clone detection
class CloneDetector(MessagePassing):
    def __init__(self, in_channels, out_channels):
        super(CloneDetector, self).__init__(aggr='max')
        self.lin = nn.Linear(in_channels, out_channels)

    def forward(self, x, edge_index):
        x = self.lin(x)
        x = F.relu(self.propagate(edge_index, x=x))
        return x

    def message(self, x_j):
        return x_j

# Define a function to create a graph representation of a code snippet
def create_code_graph(code):
    tokens = code.split()
    node_features = torch.eye(len(tokens))
    edges = [(i, j) for i in range(len(tokens)) for j in range(i+1, len(tokens)) if tokens[i] == tokens[j]]
    edge_index = torch.tensor(edges).t().contiguous()
    return Data(x=node_features, edge_index=edge_index)

# Create a dataset of code snippets
# code_snippets = ['def add(x, y):\n    return x + y\n', 
#                  'def multiply(x, y):\n    return x * y\n',
#                  'def add(x, y):\n    return x + y\n']
with open('../../data/gnn/data2.json', 'r') as f:
    data = json.load(f)
code_snippets = data.code_snippets

# Convert the code snippets to graph representations
code_graphs = [create_code_graph(code) for code in code_snippets]

# Concatenate the graph representations into a batch
batch = torch_geometric.data.Batch.from_data_list(code_graphs)

# Initialize the GNN model for code clone detection
model = CloneDetector(in_channels=batch.num_node_features, out_channels=16)

# Compute the node embeddings for each code graph
node_embeddings = model(batch.x, batch.edge_index)

# Convert the code graphs to NetworkX graphs for visualization
nx_graphs = [to_networkx(code_graph) for code_graph in code_graphs]

# Draw the NetworkX graphs to visualize the code snippets
for i, nx_graph in enumerate(nx_graphs):
    nx.draw(nx_graph, with_labels=True)
    plt.title(f'Code Snippet {i+1}')
    plt.show()

# Compute the cosine similarity between the node embeddings for each pair of code graphs
similarity_matrix = F.cosine_similarity(node_embeddings, node_embeddings)

# Print the similarity matrix
print('Code Clone Similarity Matrix:')
print(similarity_matrix)
