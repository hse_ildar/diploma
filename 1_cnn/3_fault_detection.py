import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import json

# Define the code snippets
# code_snippets = [
#     "#include <iostream>\nusing namespace std;\n\nint main()\n{\n    int x = 10;\n    cout << \"The value of x is: \" << x << endl;\n    return 0;\n}\n",
#     "#include <iostream>\nusing namespace std;\n\nint main()\n{\n    int x = 10;\n    if (x == 10)\n        cout << \"x is equal to 10\" << endl;\n    else\n        cout << \"x is not equal to 10\" << endl;\n    return 0;\n}\n",
#     "#include <iostream>\nusing namespace std;\n\nint main()\n{\n    int x = 10;\n    int y = 20;\n    int z = x + y;\n    cout << \"The sum of x and y is: \" << z << endl;\n    return 0;\n}\n"
# ]

with open('../../data/cnn/data2.json', 'r') as f:
    data = json.load(f)

print(data)

code_snippets = data.code_snippets

# Tokenize the code snippets
tokenizer = Tokenizer(num_words=10000, oov_token='<OOV>')
tokenizer.fit_on_texts(code_snippets)
sequences = tokenizer.texts_to_sequences(code_snippets)
padded_sequences = pad_sequences(sequences, maxlen=100, padding='post')

# Define the CNN model
model = keras.Sequential([
    keras.layers.Embedding(10000, 64, input_length=100),
    keras.layers.Conv1D(32, 5, activation='relu'),
    keras.layers.MaxPooling1D(pool_size=4),
    keras.layers.Conv1D(32, 5, activation='relu'),
    keras.layers.GlobalMaxPooling1D(),
    keras.layers.Dense(1, activation='sigmoid')
])

# Compile the model
model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])

# Train the model
model.fit(padded_sequences, [1, 0, 1], epochs=10)

# Evaluate the model
# test_snippets = "#include <iostream>\nusing namespace std;\n\nint main()\n{\n    int x = 10;\n    cout << \"The value of x is: \" << x << endl;\n    return 0;\n}\n"
test_snippets = data.test_snippetss
test_sequence = tokenizer.texts_to_sequences([test_snippets])
test_padded_sequence = pad_sequences(test_sequence, maxlen=100, padding='post')
prediction = model.predict(test_padded_sequence)

if prediction[0][0] > 0.5:
    print("The code snippet has a fault")
else:
    print("The code snippet is fine")