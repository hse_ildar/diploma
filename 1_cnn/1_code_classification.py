import tensorflow as tf
import numpy as np
import cv2
import clang.cindex
import json

# Define the categories for C++ code snippets
categories = ["control_flow", "data_structures", "file_io", "functions", "input_output", "loops", "pointers", "sorting", "strings"]

# Load the C++ code dataset
def load_data():
    data = {"code": [], "labels": []}
    for category in categories:
        with open(category + ".txt") as f:
            code_snippets = f.readlines()
            data["code"].extend(code_snippets)
            data["labels"].extend([category] * len(code_snippets))
    return data

# Convert a C++ code snippet to an image representation
def convert_to_image(code_snippet):
    # Parse the code snippet using the clang library
    index = clang.cindex.Index.create()
    tu = index.parse("dummy.cpp", ["-std=c++11"], unsaved_files=[("dummy.cpp", code_snippet)], options=0)
    root = tu.cursor

    # Traverse the Abstract Syntax Tree (AST) and generate an image representation
    img = np.zeros((256, 256, 3), np.uint8)
    stack = [(root, 0, 0)]
    while stack:
        node, x, y = stack.pop()
        if node.kind == clang.cindex.CursorKind.COMPOUND_STMT:
            cv2.rectangle(img, (x-2, y-2), (x+2, y+2), (255, 0, 0), -1)
        elif node.kind == clang.cindex.CursorKind.IF_STMT:
            cv2.rectangle(img, (x-2, y-2), (x+2, y+2), (0, 255, 0), -1)
        elif node.kind == clang.cindex.CursorKind.FOR_STMT:
            cv2.rectangle(img, (x-2, y-2), (x+2, y+2), (0, 0, 255), -1)
        elif node.kind == clang.cindex.CursorKind.WHILE_STMT:
            cv2.rectangle(img, (x-2, y-2), (x+2, y+2), (255, 255, 0), -1)
        elif node.kind == clang.cindex.CursorKind.FUNCTION_DECL:
            cv2.rectangle(img, (x-4, y-4), (x+4, y+4), (0, 255, 255), -1)
        for child in node.get_children():
            stack.append((child, x + 10, y + 10))
    img = cv2.resize(img, (256, 256))
    return img

# Load the dataset, here given as example
# json format { code: array of code, labels: array of labels }
with open('../../data/cnn/data.json', 'r') as f:
    data = json.load(f)

print(data)


# Preprocess the data
x_data = []
y_data = []
for i in range(len(data["code"])):
    x_data.append(convert_to_image(data["code"][i]))
    y_data.append(categories.index(data["labels"][i]))

x_data = np.array(x_data)
y_data = tf.keras.utils.to_categorical(y_data, len(categories))

# Load the pre-trained CNN model
pretrained_model = tf.keras.applications.ResNet50(weights='imagenet', include_top=False, input_shape=(256, 256, 3))

# Create a new model for C++ code classification based on the pre-trained model
x = pretrained_model.output
x = tf.keras.layers.GlobalAveragePooling2D()(x)
x = tf.keras.layers.Dense(512, activation='relu')(x)
predictions = tf.keras.layers.Dense(len(categories), activation='softmax')(x)
model = tf.keras.models.Model(inputs=pretrained_model.input, outputs=predictions)

# Freeze the weights of the pre-trained layers
for layer in pretrained_model.layers:
    layer.trainable = False

# Compile the model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Train the model
history = model.fit(x_data, y_data, batch_size=32, epochs=10, validation_split=0.2)


