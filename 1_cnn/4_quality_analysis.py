import re
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Embedding, LSTM, Dense
import json

with open('../../data/cnn/data3.json', 'r') as f:
    data = json.load(f)

print(data)

# Define the maximum sequence length and embedding dimension
MAX_SEQ_LEN = 1000
EMBEDDING_DIM = 100

# Define the LSTM units and batch size
LSTM_UNITS = 128
BATCH_SIZE = 32

# Define the coding guidelines and labels
# coding_guidelines = {
#     "Use meaningful variable names": "variable_names",
#     "Indent code properly": "indentation",
#     "Use consistent code style": "style"
# }
coding_guidelines = data.coding_guidelines
label_to_idx = {label: idx for idx, label in enumerate(coding_guidelines.values())}

# Define a C++ code snippet example
# cpp_code = '''
# #include <iostream>

# int main() {
#     int a = 5, b = 3;
#     std::cout << "Sum is: " << a+b << std::endl;
#     return 0;
# }
# '''
cpp_code = data.cpp_code

# Preprocess the C++ code snippet
def preprocess_code(code):
    # Remove comments and empty lines
    code = re.sub('//.*?\n|/\*.*?\*/', '', code, flags=re.DOTALL)
    code = "\n".join(line.strip() for line in code.splitlines() if line.strip())
    return code

cpp_code = preprocess_code(cpp_code)

# Tokenize and pad the preprocessed C++ code snippet
tokenizer = Tokenizer()
tokenizer.fit_on_texts([cpp_code])
sequences = tokenizer.texts_to_sequences([cpp_code])
x_test = pad_sequences(sequences, maxlen=MAX_SEQ_LEN)

# Define the model architecture
inputs = Input(shape=(MAX_SEQ_LEN,))
x = Embedding(input_dim=len(tokenizer.word_index) + 1, output_dim=EMBEDDING_DIM)(inputs)
x = LSTM(units=LSTM_UNITS)(x)
outputs = Dense(units=len(label_to_idx), activation="softmax")(x)
model = Model(inputs=inputs, outputs=outputs)

# Compile the model
model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

# Load the trained model weights
model.load_weights("code_quality_model_weights.h5")

# Predict the code quality of the C++ code snippet
pred = model.predict(x_test)
pred_label = list(coding_guidelines.keys())[np.argmax(pred)]

print(f"Predicted code quality: {pred_label}")