import numpy as np
import cv2
import tensorflow as tf
import json

# Step 1: Prepare the data
# Load the code snippets
# code_snippets = [
#     """
#     #include <iostream>
#     using namespace std;
    
#     int main() {
#         cout << "Hello, world!";
#         return 0;
#     }
#     """,
#     """
#     #include <iostream>
#     using namespace std;
    
#     int main() {
#         int a = 5;
#         int b = 10;
#         int c = a + b;
#         cout << "The sum of " << a << " and " << b << " is " << c << endl;
#         return 0;
#     }
#     """,
#     """
#     #include <iostream>
#     using namespace std;
    
#     int main() {
#         int a = 10;
#         if (a > 5) {
#             cout << "a is greater than 5" << endl;
#         } else {
#             cout << "a is less than or equal to 5" << endl;
#         }
#         return 0;
#     }
#     """
# ]

with open('../../data/cnn/data2.json', 'r') as f:
    data = json.load(f)

print(data)

code_snippets = data.code_snippets

# Generate labels for the code snippets
labels = data.labels  # assume the snippets are different

# Step 2: Preprocess the data
# Convert the code snippets to grayscale images
img_list = []
for code_snippet in code_snippets:
    img = cv2.cvtColor(np.array(list(code_snippet)), cv2.COLOR_RGB2GRAY)
    img_rows, img_cols = img.shape
    img_list.append(img.reshape(1, img_rows, img_cols, 1))

# Combine the images and labels into a single dataset
images = np.concatenate(img_list, axis=0)
similarity_scores = np.array(labels)

# Step 3: Train the CNN
# Define the CNN model
model = tf.keras.applications.VGG16(
    include_top=False,
    weights='imagenet',
    input_shape=(img_rows, img_cols, 1),
    pooling='max'
)

x = model.output
x = tf.keras.layers.Dense(1024, activation='relu')(x)
x = tf.keras.layers.Dense(1024, activation='relu')(x)
predictions = tf.keras.layers.Dense(3, activation='softmax')(x)

model = tf.keras.Model(inputs=model.input, outputs=predictions)

# Compile the model
model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(images, similarity_scores, epochs=10)

# Step 4: Evaluate the model
# Evaluate the model on the sample code snippets
# test_snippets = [
#     """
#     #include <iostream>
#     using namespace std;
    
#     int main() {
#         cout << "Hello, world!";
#         return 0;
#     }
#     """,
#     """
#     #include <iostream>
#     using namespace std;
    
#     int main() {
#         int a = 5;
#         int b = 10;
#         int c = a + b;
#         cout << "The sum of " << a << " and " << b << " is " << c << endl;
#         return 0;
#     }
#     """
# ]
test_snippets = data.test_snippets
test_labels = data.test_labels

# Generate test data
test_img_list = []
for test_snippet in test_snippets:
    test_img = cv2.cvtColor(np.array(list(test_snippet)), cv2.COLOR_RGB2GRAY)
    test_img_rows, test_img_cols = test_img.shape
    test_img_list.append(test_img.reshape(1, test_img_rows, test_img_cols, 1))

test_images = np.concatenate(test_img_list, axis=0)
test_similarity_scores = np.array(test_labels)

# Evaluate the model on the test data
test_loss, test_acc = model.evaluate(test_images, test_similarity_scores)
print('Test accuracy:', test_acc)