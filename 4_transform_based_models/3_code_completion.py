import pandas as pd
import torch
from transformers import AutoTokenizer, AutoModel

# Load the C++ dataset
df = pd.read_csv('../../data/TBM/data3.csv')

# Initialize the tokenizer
tokenizer = AutoTokenizer.from_pretrained('bert-base-cased')

# Tokenize the input code snippets
inputs = df['code_snippet'].apply(lambda x: tokenizer.encode(x, add_special_tokens=True))

# Define the target sequences
targets = df['target_sequence']

# Preprocess the target sequences
target_inputs = targets.apply(lambda x: tokenizer.encode(x, add_special_tokens=True))
target_outputs = target_inputs[:, 1:]
target_inputs = target_inputs[:, :-1]

# Define the model architecture
class TransformerModel(torch.nn.Module):
    def __init__(self, model_name):
        super().__init__()
        self.encoder = AutoModel.from_pretrained(model_name)
        self.decoder = torch.nn.Linear(self.encoder.config.hidden_size, tokenizer.vocab_size)

    def forward(self, inputs, attention_mask):
        outputs = self.encoder(inputs, attention_mask=attention_mask)
        logits = self.decoder(outputs.last_hidden_state)
        return logits

# Initialize the model
model = TransformerModel('bert-base-cased')

# Define the loss function and optimizer
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)

# Train the model
for epoch in range(num_epochs):
    for input_batch, target_input_batch, target_output_batch in zip(inputs, target_inputs, target_outputs):
        optimizer.zero_grad()

        # Forward pass
        logits = model(input_batch.unsqueeze(0), attention_mask=input_batch.ne(0))[0][:-1]
        loss = criterion(logits, target_output_batch)

        # Backward pass and optimization
        loss.backward()
        optimizer.step()

        # Print the loss
        print(f'Epoch {epoch}, loss {loss.item():.4f}')

# Generate code completions for a new input code snippet
new_input = 'int main() {\n    std::cout << "Hello, world!";\n    re'
new_input_tokens = tokenizer.encode(new_input, add_special_tokens=True)
new_input_inputs = torch.tensor([new_input_tokens])
new_input_mask = torch.tensor([[1] * len(new_input_tokens)])

with torch.no_grad():
    output_tokens = model.generate(input_ids=new_input_inputs, attention_mask=new_input_mask)
    output_sequence = tokenizer.decode(output_tokens[0], skip_special_tokens=True)

print(f'Input code snippet: {new_input}')
print(f'Generated code completion: {output_sequence}')
