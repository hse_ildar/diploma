import torch
import transformers
import numpy as np
import json

# Load the pre-trained Transformer model and tokenizer
model_name = 'bert-base-cased'
model = transformers.AutoModelForSeq2SeqLM.from_pretrained(model_name)
tokenizer = transformers.AutoTokenizer.from_pretrained(model_name)

# Define a function for generating code summaries
def summarize_code(code_snippet):
    # Tokenize the code snippet
    tokenized_code = tokenizer.encode(code_snippet, return_tensors='pt')
    
    # Generate the summary using the model
    summary_ids = model.generate(tokenized_code,
                                  max_length=128,
                                  num_beams=4,
                                  length_penalty=2.0,
                                  early_stopping=True)
    
    # Decode the summary from the token IDs
    summary = tokenizer.decode(summary_ids.squeeze(), skip_special_tokens=True)
    
    return summary

# Example usage
# c_plus_plus_code = "int main() { cout << 'Hello, World!' << endl; return 0; }"
with open('../../data/TBM/data1.json', 'r') as f:
    data = json.load(f)

print(data)
c_plus_plus_code = data.cpp

summary = summarize_code(c_plus_plus_code)
print(summary)
