import tensorflow as tf
import numpy as np
import pandas as pd

# Load the pre-trained Transformer-based model (e.g., BERT)
model = tf.keras.models.load_model('../../data/TBM/BERT_MODEL')

# Load the C++ code dataset
df = pd.read_csv('../../data/TBM/dataset4.csv')

# Encode the code snippets as input sequences for the model
input_sequences = []
for code_snippet in df['code_snippet']:
    input_sequence = ' '.join(['[CLS]', code_snippet, '[SEP]'])
    input_sequences.append(input_sequence)

# Generate embeddings for the input sequences using the pre-trained model
embeddings = []
for input_sequence in input_sequences:
    input_ids = tokenizer.encode(input_sequence, add_special_tokens=False)
    input_ids = tf.keras.preprocessing.sequence.pad_sequences([input_ids], maxlen=max_seq_length, dtype='int32', padding='post', truncating='post')
    input_mask = np.ones_like(input_ids)
    segment_ids = np.zeros_like(input_ids)
    embedding = model.predict([input_ids, input_mask, segment_ids])[0]
    embeddings.append(embedding)

# Perform a similarity search using the embeddings and a query
def search(query, k=10):
    query_embedding = model.predict(query)
    similarities = []
    for i, embedding in enumerate(embeddings):
        similarity = cosine_similarity(query_embedding, embedding)
        similarities.append((i, similarity))
    similarities.sort(reverse=True, key=lambda x: x[1])
    top_k_indices = [i for i, _ in similarities[:k]]
    top_k_snippets = df.loc[top_k_indices, 'code_snippet'].tolist()
    return top_k_snippets

# Example usage
query = 'sorting algorithm in c++'
query_embedding = model.predict(query)
top_k_snippets = search(query_embedding, k=10)
print(top_k_snippets)
