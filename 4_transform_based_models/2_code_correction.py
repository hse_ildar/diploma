import torch
from transformers import AutoTokenizer, AutoModelForSeq2SeqLM

# Load tokenizer
tokenizer = AutoTokenizer.from_pretrained("microsoft/codebert-base")

# Load model
model = AutoModelForSeq2SeqLM.from_pretrained("microsoft/codebert-base")

# Set device
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model.to(device)

# Load C++ code snippets
with open("../../data/TBM/data_snippet2.txt", "r") as f:
    code_snippets = [line.strip() for line in f]

# Preprocess input
input_ids = tokenizer.encode("Fix the syntax error in the following C++ code snippet:\n" + code_snippets[0] + "\n", return_tensors="pt").to(device)

# Generate corrected code
output_ids = model.generate(input_ids, max_length=1024)

# Decode output
output_snippet = tokenizer.decode(output_ids[0], skip_special_tokens=True)
print(output_snippet)
